using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillCard : MonoBehaviour
{
    public GameObject button;
    public GameObject costText;
    public GameObject levelText;
    public GameObject toolTipText;
    public GameObject blackImage;
    public GManager.StatusType statusType;
    public SkillCardModel model;
    public AudioClip acceptSE;

    private AudioSource audioSource;

    private float cost;
    private float point;
    private float level;      
    private bool isActive = false;
    private float speedLimit() 
    {
        return 120f;
    }
    
    // Start is called before the first frame update
    void Start()
    {        
        button.SetActive(false);
        blackImage.SetActive(false);
        audioSource = GetComponent<AudioSource>();
    }

    public void Init(string cardName)
    {
        model = new SkillCardModel(cardName);
        this.Show(model);
    }

    public void Show(SkillCardModel cardModel)
    {
        this.cost = cardModel.cost;
        this.point = GManager.instance.skillCost;
        this.level = cardModel.level;
        this.statusType = cardModel.statusType;
        GetComponent<Image>().sprite = cardModel.sprite;
        costText.GetComponent<Text>().text = cost.ToString();
        levelText.GetComponent<Text>().text = "Lv." + level.ToString();
    }


    public void New(float _cost,float _point,float _level,GManager.StatusType _statusType)
    {
        this.cost = _cost;
        this.point = GManager.instance.skillCost;
        this.level = _level;
        this.statusType = _statusType;
        costText.GetComponent<Text>().text = cost.ToString();
        levelText.GetComponent<Text>().text = "Lv." + level.ToString();
    }


    // Update is called once per frame
    void Update()
    {
        //購入可能になった時
        if((GManager.instance.skillPoint >= this.cost) && (IsCanPurchase()) && GManager.mode != GManager.Mode.clear)
        {
            isActive = true;
            blackImage.SetActive(false);
        }
        else
        {
            isActive = false;
            blackImage.SetActive(true);
        }
    }

    public void OnMouseOver()
    {
        //ツールチップ表示
        //button.SetActive(true);
        if (GManager.mode != GManager.Mode.clear)
        {
            GManager.instance.DisplaySkillEffect(SkillDescription());
        }
    }

    private bool IsCanPurchase()
    {
        switch (this.statusType)
        {
            case GManager.StatusType.power:            
            case GManager.StatusType.hp:
            case GManager.StatusType.heal:
                if(point>= 1000000)
                {
                    return false;
                }
                break;

            case GManager.StatusType.speed:
                if(GManager.instance.player.speed >= speedLimit())
                {
                    return false;
                }
                break;

            case GManager.StatusType.enemyLebel:
                if(this.level == 10)
                {
                    return false;
                }
                break;
        }
        return true;
    }

    public string SkillDescription()
    {
        string description = string.Empty;
        float pointForDisplay = this.point;
        switch (this.statusType)
        {
            case GManager.StatusType.enemyLebel:
                description = "敵のレベル";
                break;
            case GManager.StatusType.heal:
                description = "自動回復";
                break;
            case GManager.StatusType.hp:
                description = "最大HP";
                break;
            case GManager.StatusType.power:
                description = "攻撃力";
                break;
            case GManager.StatusType.speed:
                description = "移動速度";
                float sum = GManager.instance.player.speed + this.point;
                float upLimit = speedLimit();
                if (sum >= upLimit)
                {
                    pointForDisplay = upLimit - GManager.instance.player.speed;
                }
                break;
        }

        description += "が" + pointForDisplay.ToString() + "上昇します";

        if(this.level == 0)
        {
            description = "敵のレベルを下げます";
        }

        if (IsCanPurchase() == false)
        {
            description = "上限に達しました";
        }

        return description;
    }

    public void OnMouseExit()
    {
        //ツールチップ非表示
        //button.SetActive(false);
        GManager.instance.DisplaySkillEffect("");
    }

    public void OnClick()
    {
        if (isActive == false) { return; }
        isActive = false;        

        if ( this.level != 0)
        {
            audioSource.PlayOneShot(acceptSE);
            GManager.instance.SetSkillPoint(-this.cost);
            GManager.instance.ChangeStatusForSkillCard(this.statusType, this.point,this.level);
            //GManager.instance.UpSkillCost();
            LevelUp();
        }
        else if(this.statusType == GManager.StatusType.enemyLebel)
        {
            if(GManager.instance.enemyLevel > 1)
            {
                audioSource.PlayOneShot(acceptSE);
                LevelUp();
            }
        }
    }

    public void LevelUp()
    {
        if(this.level != 0)
        {
            level += 1;
            levelText.GetComponent<Text>().text = "Lv." + level.ToString();
        }

        switch(this.statusType)
        {
            case GManager.StatusType.hp:
            case GManager.StatusType.heal:
            case GManager.StatusType.power:
            case GManager.StatusType.speed:
                this.cost *= 1.2f;        
                this.cost = Mathf.Ceil(this.cost);
                this.point *= 1.1f;
                this.point = Mathf.Ceil(this.point);
                if(this.statusType == GManager.StatusType.speed && GManager.instance.player.speed >= speedLimit())
                {
                    GManager.instance.player.speed = speedLimit();
                }
                break;
            
            case GManager.StatusType.enemyLebel:
                if (this.level != 0)
                {
                    this.cost *= 3;
                    this.cost = Mathf.Round(this.cost);
                }
                else
                {
                    //レベルダウン
                    GManager.instance.enemyLevel -= 1;
                    GManager.instance.enemyList[0].LevelDown();

                    foreach (GameObject obj in GManager.instance.skillCardList)
                    {
                        SkillCard skillCard = obj.GetComponent<SkillCard>();
                        if ( skillCard.statusType == GManager.StatusType.enemyLebel && skillCard.level != 0)
                        {
                            skillCard.level -= 1;
                            skillCard.cost /= 3;
                            skillCard.cost = Mathf.Round(skillCard.cost);
                            if(skillCard.cost <= 3) { skillCard.cost = 3; }
                            skillCard.costText.GetComponent<Text>().text = skillCard.cost.ToString();
                            skillCard.levelText.GetComponent<Text>().text = "Lv." + skillCard.level.ToString();
                        }
                    }
                }
                break;
        }

        costText.GetComponent<Text>().text = cost.ToString();
    }

}
