using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //[SerializeField]
    //GameObject enemyPrefab = null;
    //[SerializeField]
    //GameObject enemyPrefab2 = null;
    [SerializeField]
    public List<GameObject> enemyPrefabs;

    public float defaultWaitTime;
    //private bool isSpawning = false;
    //public List<EnemySpawn> enemySpawnList = new List<EnemySpawn>();
    public bool isEnableSpawn = true; //敵を倒すと次の敵をスポーンする

    GameObject playerObj;
    Player player;
    Transform playerTransform;
    float differenceX;

    //Coroutine timer;
    // Start is called before the first frame update
    void Start()
    {
        playerObj = GameObject.FindGameObjectWithTag("Player");
        player = playerObj.GetComponent<Player>();
        playerTransform = playerObj.transform;
        differenceX = playerTransform.position.x - transform.position.x;
    }

    private void LateUpdate()
    {
        Move();
        Spawn();
    }

    private void Move()
    {
        float moveX = playerTransform.position.x - transform.position.x - differenceX;
        transform.position = new Vector3(transform.position.x + moveX, transform.position.y, transform.position.z);
    }

    private void Spawn()
    {
        if (isEnableSpawn == false) { return; }
        isEnableSpawn = false;

        GameObject enemyInstance = null;

        if (enemyPrefabs.Count < GManager.instance.enemyLevel) { return; }

        GameObject enemyPrefab = enemyPrefabs.Find(m => m.GetComponent<Enemy>().level == GManager.instance.enemyLevel);

        enemyInstance = Instantiate(enemyPrefab, transform.position, Quaternion.identity);

        if (enemyInstance == null) { return; }

        //生成時の距離
        Vector3 vector3 = enemyInstance.transform.position;
        vector3.x += enemyInstance.GetComponent<Enemy>().distance;
        enemyInstance.transform.position = vector3;

        GManager.instance.enemyList.Add(enemyInstance.GetComponent<Enemy>());
    }
}
