using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public GameObject scoreText;
    public GameObject rankText;
    public GameObject canvas;
    private float playTime = 0;
    private float score = 0;
    private string rank = string.Empty;    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GManager.instance.isCleared == false)
        {
            //クリアしていない場合
            playTime += Time.deltaTime;            
        }
        else
        {
            //クリア後

            float maxScore = 1600f;           
            score = maxScore - playTime;
            score -= GManager.instance.deathCount * 100f;
            score = Mathf.Round(score);

            if (score < 0) { score = 0; }

            if(score > 1000f)
            {
                rank = "SSS";
            }
            else if (score > 900f)
            {
                rank = "SS";
            }
            else if (score > 800f)
            {
                rank = "S";
            }
            else if (score > 700f)
            {
                rank = "A";
            }
            else if (score > 600f)
            {
                rank = "B";
            }
            else if (score > 500f)            
            {
                rank = "C";
            }
            else if (score > 400f)
            {
                rank = "D";
            }
            else if (score > 300f)
            {
                rank = "E";
            }
            else
            {
                rank = "F";
            }

            GManager.mode = GManager.Mode.clear;
            canvas.SetActive(true);
            scoreText.GetComponent<Text>().text = score.ToString();
            rankText.GetComponent<Text>().text = rank;
        }
    }
}
