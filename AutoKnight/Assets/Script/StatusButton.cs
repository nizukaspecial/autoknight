using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusButton : MonoBehaviour
{
    public Button StatusButtonObj;
    public GameObject StatusButtonText;
    public GameObject StatusText;

    // Start is called before the first frame update
    void Start()
    {
        if (StatusButtonObj.tag == "Button_Status")
        {
            OnClick();
        }   
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
        GManager.instance.ClickDisplayButton(StatusButtonObj.tag);
    }
}
