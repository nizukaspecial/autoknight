using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGround : MonoBehaviour
{
    Vector3 cameraRectMin;
    private float width;

    // Start is called before the first frame update
    void Start()
    {
        cameraRectMin = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, Camera.main.transform.position.z));
        GManager.instance.backGroundList.Add(this);
        width = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void Update()
    {
        if(GManager.mode == GManager.Mode.move || GManager.mode == GManager.Mode.clear)
        {
            move();
        }
    }

    void move()
    {

        Vector3 myViewport = Camera.main.WorldToViewportPoint(transform.position);

        //カメラの左側から出たら右側に移動
        //if (transform.position.x < (cameraRectMin.x - Camera.main.transform.position.x) * 3)
        if (myViewport.x < -0.55f)
        {
            transform.position = new Vector2(transform.position.x + width * 2 - 0.1f, transform.position.y);
        }

    }
}
