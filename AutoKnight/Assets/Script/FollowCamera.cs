using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    // Start is called before the first frame update

    GameObject playerObj;
    Player player;
    Transform playerTransform;
    float differenceX;

    void Start()
    {
        playerObj = GameObject.FindGameObjectWithTag("Player");
        player = playerObj.GetComponent<Player>();
        playerTransform = playerObj.transform;
        differenceX = playerTransform.position.x - transform.position.x;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        MoveCamera();
    }

    void MoveCamera()
    {
        float moveX = playerTransform.position.x - transform.position.x - differenceX;
        transform.position = new Vector3(transform.position.x + moveX, transform.position.y,transform.position.z);
    }
}
