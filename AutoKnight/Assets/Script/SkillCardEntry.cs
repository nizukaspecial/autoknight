using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="CreateCard")]

public class SkillCardEntry : ScriptableObject
{
    public float cost;
    public float point;
    public float level;
    public GManager.StatusType statusType;
    public Sprite sprite;
}
