using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Player : MonoBehaviour
{

    public float downTime;
    public float power;
    public float hp;
    public float maxhp;
    public float healPoint;
    public float pointXBeforeStageCreate;
    public float speed;
    public GameObject damageTextPrefab;
    public GameObject healTextPrefab;
    public GameObject canvas;

    public AudioClip zapSE1;
    public AudioClip zapSE2;
    private GameObject damageTextInstance = null;
    private GameObject healTextInstance = null;
    private AudioSource audioSource;
    private Rigidbody2D rb;
    private Animator animator = null;   
    private float elapsedTime = 0;
    private float elapsedTimeManual = 0;
    private bool isZap = false;
    private bool isFirstZap = true;
    public bool isFirstZapProperty
    {
        get { return isFirstZap; }
        private set { isFirstZap = value; }
    }

    private Enemy enemy = null;
    
    private GameObject zapEffect = null;
    private GameObject zapEffect2 = null;
    private bool isAfterZap1 = false;
    private float accelPoint = 0;
    private float healtime = 0;

    //[HideInInspector] public Enemy enemy = null;
    public void SetEnemy(Enemy _enemy)
    {
        this.enemy = _enemy;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        this.zapEffect = GameObject.Find("ZapEffect");
        this.zapEffect.SetActive(false);

        this.zapEffect2 = GameObject.Find("ZapEffect2");
        this.zapEffect2.SetActive(false);

        rb = GetComponent<Rigidbody2D>();

        
        pointXBeforeStageCreate = transform.position.x;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GManager.mode == GManager.Mode.move)
        {
            animator.SetBool("Run", true);
            rb.velocity = new Vector2(speed + accelPoint, rb.velocity.y); //走る
        }
        else if (GManager.mode == GManager.Mode.battle)
        {
            rb.velocity = new Vector2(0, 0); //止まる
            if (enemy != null)
            {
                animator.SetBool("Run", false);
                BattleAction();
            }
            else
            {
                //敵を倒した場合
                elapsedTime += Time.deltaTime;
                if (elapsedTime > 0.3f)
                {
                    EndBattle();
                }
            }
        }
        else if (GManager.mode == GManager.Mode.clear)
        {
            animator.SetBool("Run", true);
            animator.Play("Player_Run");
            rb.velocity = new Vector2(3 + accelPoint, rb.velocity.y); //走る
        }

        //ダメージ表示の移動
        if (damageTextInstance != null)
        {
            damageTextInstance.transform.position += new Vector3(0, 0.01f, 0);
        }

        //自動回復
        if (GManager.mode != GManager.Mode.down && this.healPoint > 0)
        {
            healtime += Time.deltaTime;
            if (healtime >= 5f)
            {
                healtime = 0;
                if (this.hp < this.maxhp)
                {
                    this.hp += this.healPoint;
                    if (this.hp > this.maxhp)
                    {
                        this.hp = this.maxhp;
                    }
                    DisplayHeal(this.healPoint);
                }
            }
        }

        //回復表示の移動
        if (healTextInstance != null)
        {
            healTextInstance.transform.position += new Vector3(0, 0.01f, 0);
        }

        //再描画
        GManager.instance.setAllStatus();
    }

    void BattleAction()
    {
        if(isFirstZap == true)
        {
            Attack(false);
        }

        elapsedTime += Time.deltaTime;

        if (isZap == true)
        {
            if (elapsedTime > 0.2f)
            {
                animator.Play("Player_Stay");
                isZap = false;
                elapsedTime = 0f;
            }
        }
        else
        {
            if (elapsedTime > downTime)
            {
                Attack(false);
            }
        }
    }

    void Attack(bool IsClick)
    {
        float damage = power;
        elapsedTimeManual += Time.deltaTime;
        if (IsClick == true && elapsedTimeManual < 0.2f && isFirstZap == false) 
        {            
            damage /= 5;
            damage = Mathf.Round(damage);
            if(damage <= 1)
            {
                damage = 1;
            }
        }
        else
        {
            elapsedTimeManual = 0f;
            isFirstZap = true;
        }


        if (isAfterZap1 == false)
        {
            audioSource.PlayOneShot(zapSE1);
            animator.Play("Player_Battle");
            isAfterZap1 = true;
        }
        else
        {
            audioSource.PlayOneShot(zapSE2);
            animator.Play("Player_Battle2");
            isAfterZap1 = false;
        }
        StartCoroutine(ZapEffectTimer());
        
        isFirstZap = false;
        isZap = true;
        elapsedTime = 0f;
        if(enemy != null)
        {
            enemy.ReceiveDamage(damage);
        }
    }

    IEnumerator ZapEffectTimer()
    {
        if (isAfterZap1 == false)
        {
            this.zapEffect.SetActive(true);
            this.enemy.Zapped();
            yield return new WaitForSeconds(0.1f);
            this.zapEffect.SetActive(false);
        }
        else
        {
            this.zapEffect2.SetActive(true);
            this.enemy.Zapped();
            yield return new WaitForSeconds(0.1f);
            this.zapEffect2.SetActive(false);
        }
    }

    public void EndBattle()
    {
        isFirstZap = true;
        isZap = false;
        elapsedTime = 0f;
        animator.Play("Player_Run");
        GManager.mode = GManager.Mode.move;
    }

    public void DisplayDamage(float damage)
    {
        this.damageTextPrefab.GetComponent<Text>().text = "-" + damage.ToString();
        this.healTextPrefab.GetComponent<Text>().color = new Color(1.0f, 0f, 0f, 1.0f);
        this.damageTextInstance = Instantiate(this.damageTextPrefab, this.canvas.transform);
        this.damageTextInstance.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
        Destroy(this.damageTextInstance, 1f);
    }

    public void DisplayHeal(float regene)
    {
        this.healTextPrefab.GetComponent<Text>().text = "+" + regene.ToString();
        this.healTextPrefab.GetComponent<Text>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        this.healTextInstance = Instantiate(this.healTextPrefab, this.canvas.transform);
        this.healTextInstance.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
        Destroy(this.healTextInstance, 1f);
    }

    public void Down()
    {
        animator.Play("Player_Down");
        GManager.instance.deathCount += 1;
        StartCoroutine(UntilResurrectionTimer());
    }

    IEnumerator UntilResurrectionTimer()
    {
        yield return new WaitForSeconds(5f);
        if (this.enemy == null)
        {
            animator.Play("Player_Run");
            GManager.mode = GManager.Mode.move;        
        }
        else
        {
            isFirstZap = true;
            GManager.mode = GManager.Mode.battle;
        }
        this.hp = this.maxhp;
        GManager.instance.setAllStatus();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            GManager.mode = GManager.Mode.battle;
            enemy = collision.gameObject.GetComponent<Enemy>();
            enemy.SetIsBattle(true);
        }
        else if(collision.tag == "StageMaker")
        {
            GManager.instance.CreateStage();
        }
    }

    public void OnSortiePointClick()
    {
        if(GManager.mode == GManager.Mode.move)
        {
            //敵のスポーン速度を上げる、敵に近づく
            //GManager.instance.speedUPEnemyMove();
            StartCoroutine(SpeedUP());
        }
        else if(GManager.mode == GManager.Mode.battle & this.enemy != null)
        {
            //切る
            Attack(true);
        }
        else if(GManager.mode == GManager.Mode.down)
        {
            //復活時間を短縮する（不要？）
        }
    }

    IEnumerator SpeedUP()
    {
        this.accelPoint += 7f;
        yield return new WaitForSeconds(0.3f);
        this.accelPoint -= 7f;
    }
    
}
