using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : MonoBehaviour   
{
    public float level;
    public float power;
    public float hp;
    public float armer;
    public float skillPoint;
    public float coolTime;
    public float speedUpCount = 0;
    public float distance;
    public AudioClip zapSE;
    public Slider slider;

    [SerializeField]
    GameObject damageTextPrefab;

    [SerializeField]
    GameObject canvas;
    
    private GameObject damageTextInstance = null;
    private AudioSource audioSource;
    private Rigidbody2D rb = null;
    private Player player = null;
    private Animator animator = null;
    private bool _isBattle { get; set; }
    private bool isAttacking = false;
    private bool isAlive = true;
    private bool isEvenZapped = false;

    public void SetIsBattle(bool value)
    {
        this._isBattle = value;    
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        GameObject childGameObject = transform.Find("ZappedEffect").gameObject;
        animator = childGameObject.GetComponent<Animator>();
        childGameObject.SetActive(true);
        audioSource = GetComponent<AudioSource>();
        float maxHp = this.hp;
        slider.maxValue = maxHp;
    }

    // Update is called once per frame
    void Update()
    {
        if(GManager.mode == GManager.Mode.move)
        {         
        }
        else if(GManager.mode == GManager.Mode.battle && player != null)
        {
            if (isAttacking == false & _isBattle == true & player.isFirstZapProperty == false)
            {
                StartCoroutine(BattleActionTimer());
            }
        }
        slider.value = this.hp;

        if(damageTextInstance != null)
        {
            damageTextInstance.transform.position += new Vector3(0, 0.01f, 0);
        }
    }

    IEnumerator BattleActionTimer()
    {
        isAttacking = true;
        if(isAlive == true)
        {
            StartCoroutine(AttackMotion());
            GManager.instance.ReducePlayerHP(this.power);
        }
        yield return new WaitForSeconds(this.coolTime);
        isAttacking = false;
    }

    IEnumerator AttackMotion()
    {
        Vector3 startPos = transform.position;
        Vector3 endPos = transform.position;
        endPos.x -= 0.5f;
        
        transform.position = Vector3.Lerp(startPos, endPos, 1f);
        yield return new WaitForSeconds(0.2f);
        transform.position = Vector3.Lerp(endPos, startPos, 1f);

        audioSource.PlayOneShot(zapSE);
    }

    public void ReceiveDamage(float damage)
    {
        damage -= armer;
        DisplayDamage(damage);
        if (damage <= 0) { return; }
        hp -= damage;

        if (hp <= 0)
        {
            if(this.level == 10) 
            { 
                GManager.instance.isCleared = true; 
            }

            isAlive = false;
            GManager.instance.enemyList.RemoveAt(0);
            Destroy(this.gameObject);
            player.SetEnemy(null);            

            if(GManager.instance.isCleared == false)
            {
                GManager.instance.SetSkillPoint(this.skillPoint);
                GManager.instance.PermissionSpawn();
            }
        }
    }

    public void LevelDown()
    {        
        if(this.level > GManager.instance.enemyLevel)
        {
            isAlive = false;
            GManager.instance.enemyList.RemoveAt(0);
            Destroy(this.gameObject);
            player.SetEnemy(null);
            GManager.instance.PermissionSpawn();
        }
    }


    private void DisplayDamage(float damage)
    {
        damageTextPrefab.GetComponent<Text>().text = "-" + damage.ToString();
        this.damageTextInstance = Instantiate(damageTextPrefab, canvas.transform);
        this.damageTextInstance.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 0.5f, this.transform.position.z);
        Destroy(this.damageTextInstance, 1f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {            
            player = collision.gameObject.GetComponent<Player>();
        }
    }

    public void Zapped()
    {
        StartCoroutine(ZappedEffect());
    }

    IEnumerator ZappedEffect()
    {
        if (isEvenZapped == false)
        {
            isEvenZapped = true;
            yield return new WaitForSeconds(0.1f);
        }
        else
        {
            isEvenZapped = false;
            yield return new WaitForSeconds(0.1f);
        }
    }

}
