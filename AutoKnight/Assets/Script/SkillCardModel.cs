using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillCardModel
{
    public float cost;
    public float point;
    public float level;
    public GManager.StatusType statusType;
    public Sprite sprite;

    public SkillCardModel(string cardName)
    {
        SkillCardEntry cardModel = Resources.Load<SkillCardEntry>("CardList/" + cardName);
        sprite = cardModel.sprite;
        cost = cardModel.cost;
        point = cardModel.point;
        level = cardModel.level;
        statusType = cardModel.statusType;
    }
}
