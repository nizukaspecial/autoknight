using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class GManager : MonoBehaviour
{
    public static GManager instance { get; private set; }
    public GameObject skillPointText;
    public GameObject hpText;
    public GameObject attackText;
    public GameObject speedText;
    public GameObject healText;
    public GameObject enemyLevelText;
    public GameObject stage;
    public GameObject skillCardPrefab;
    public GameObject statusButton;   
    public List<GameObject> statusUpButtonList;
    public GameObject statusText;
    public GameObject skillCardPanel;
    public GameObject skillCostText;
    public GameObject skillEffectText;

    public List<GameObject> skillCardList = new List<GameObject>();
    public Player player;
    public List<Enemy> enemyList = new List<Enemy>();
    public List<BackGround> backGroundList = new List<BackGround>();
    public EnemySpawner enemySpawner;
    public float skillPoint;
    public float skillCost = 1;
    public float enemyLevel = 1;

    public bool isCleared = false;
    public float deathCount = 0;
    
    public enum StatusType
    {
        hp = 0,
        power = 1,
        speed = 2,
        heal = 3,
        enemyLebel = 4
    }

    public static Mode mode;
    public enum Mode
    {
        move = 0,
        battle = 1,
        down = 2,
        clear = 3
    }    

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        setAllStatus();

        createSkillCard("Card_Power1");
        createSkillCard("Card_Speed1");
        createSkillCard("Card_HP1");
        createSkillCard("Card_Heal1");
        createSkillCard("Card_EnemyLevel1");
        createSkillCard("Card_EnemyLevelDown");

        SetSkillCost(skillCost);
    }

    public void setAllStatus()
    {
        SetStatus(hpText, "HP",player.hp,StatusType.hp);
        SetStatus(attackText, "Attack",player.power,StatusType.power);
        SetStatus(speedText, "Speed", player.speed,StatusType.speed);
        SetStatus(healText, "Heal",player.healPoint,StatusType.heal);
        SetStatus(enemyLevelText, "EnemyLevel", enemyLevel,StatusType.enemyLebel);
    }    

    private void createSkillCard(string cardName)
    {
        GameObject card = Instantiate(skillCardPrefab, skillCardPanel.transform);
        card.GetComponent<SkillCard>().Init(cardName);
        card.SetActive(false);
        card.SetActive(true);
        skillCardList.Add(card);
    }

    public void SetStatus(GameObject gameObject,string strStatus,float status,StatusType statusType)
    {
        Text objText = gameObject.GetComponent<Text>();
        objText.text = strStatus + " : " + status.ToString("0");
        if (statusType == StatusType.hp)
        {
            objText.text += " / " + player.maxhp;
        }
    }

    public void ChangeStatusForSkillCard(StatusType _statusType, float _point, float _level)
    {
        switch(_statusType)
        {
            case StatusType.hp:
                player.maxhp += _point;                
                if(mode != Mode.down)
                {
                    player.hp += _point;
                }
                SetStatus(hpText, "HP", player.hp,StatusType.hp);
                break;
            case StatusType.power:
                player.power += _point;
                SetStatus(attackText, "Power", player.power,StatusType.power);
                break;
            case StatusType.speed:
                player.speed += _point;
                SetStatus(speedText, "Speed", player.speed,StatusType.speed);
                break;
            case StatusType.heal:
                player.healPoint += _point;
                SetStatus(healText, "Heal", player.healPoint,StatusType.heal);
                break;
            case StatusType.enemyLebel:
                if (_level != 0)
                {
                    enemyLevel += 1;
                    SetStatus(enemyLevelText, "EnemyLevel", enemyLevel,StatusType.enemyLebel);
                }
                else
                {
                    //レベルダウン
                    enemyLevel -= 1;
                    SetStatus(enemyLevelText, "EnemyLevel", enemyLevel,StatusType.enemyLebel);
                }
                break;
        }
    }

    public void SetSkillPoint(float point)
    {
        this.skillPoint += point;
        Text objText = skillPointText.GetComponent<Text>();
        objText.text = "Skill Point : " + this.skillPoint.ToString("0");
    }

    public void SetSkillCost(float point)
    {        
        Text objText = skillCostText.GetComponent<Text>();
        objText.text = "Skill Cost : " + point.ToString("0");
    }

    public void DisplaySkillEffect(string effect)
    {
        skillEffectText.GetComponent<Text>().text = effect;
    }


    public void PermissionSpawn()
    {
        this.enemySpawner.isEnableSpawn = true;
    }

    public void ReducePlayerHP(float damage)
    {
        player.hp -= damage;
        player.DisplayDamage(damage);
        if (player.hp <= 0) 
        { 
            player.hp = 0;
            player.Down();
            GManager.mode = Mode.down;
        }
        setAllStatus();
    }

    public void CreateStage()
    {
        float moveDistance = player.transform.position.x - player.pointXBeforeStageCreate;
        player.pointXBeforeStageCreate = player.transform.position.x;
        Vector3 vector3 = stage.GetComponent<Transform>().position;
        vector3.x += moveDistance;
        stage.GetComponent<Transform>().position = vector3;
    }

    public void ClickDisplayButton(string tagName)
    {
        //一旦全てオフにする
        statusButton.GetComponentInChildren<Image>().color = new Color(1f, 1f, 1f);
        foreach(GameObject gameObject in this.statusUpButtonList)
        {
            gameObject.GetComponentInChildren<Image>().color = new Color(1f, 1f, 1f);
        }

        statusText.SetActive(false);
        foreach (GameObject gameObject in this.skillCardList)
        {
            gameObject.SetActive(false);
        }

        //とりあえず全部オンにしとく（仮ソース）
        statusText.SetActive(true);
        foreach (GameObject gameObject in this.skillCardList)
        {
            gameObject.SetActive(true);
        }

    }
}
