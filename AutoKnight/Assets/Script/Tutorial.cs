using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public GameObject tutorialObject;
    public GameObject nextButton;
    public GameObject beforeButton;
    public GameObject closeButton;
    public List<GameObject> tutorialImages;
    private float pageIndex = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickTutorialButton()
    {
        tutorialObject.SetActive(true);
        pageIndex = 0;
        viewPage();
    }

    public void ClickCloseButton()
    {
        tutorialObject.SetActive(false);
        pageIndex = 0;
    }

    public void ClickNextButton()
    {
        if(tutorialImages.Count - 1 >= pageIndex + 1)
        {
            pageIndex += 1;
            viewPage();
        }
        else
        {
            ClickCloseButton();
        }
    }

    public void ClickBeforeButton()
    {
        if (pageIndex - 1 >= 0)
        {
            pageIndex -= 1;
            viewPage();
        }
        else
        {
            ClickCloseButton();
        }
    }

    public void viewPage()
    {
        foreach (GameObject tutorialImage in tutorialImages)
        {
            if (tutorialImages.IndexOf(tutorialImage) == pageIndex)
            {
                tutorialImage.SetActive(true);
            }
            else
            {
                tutorialImage.SetActive(false);
            }
        }
    }

}
